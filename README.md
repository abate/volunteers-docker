
Ansible based deployment

from the ansible directory :

    ansible-playbook site.yml -t deploy --vault-password-file ~/.vms-vault_pass.txt

the ``~/.vms-vault_pass.txt`` contains the vault password
